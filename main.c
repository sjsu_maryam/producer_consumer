#include <stdio.h>

#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"

#include "board_io.h"
#include "common_macros.h"
#include "periodic_scheduler.h"
#include "sj2_cli.h"

#include "adc.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "pwm1.h"
#include "queue.h"

typedef enum { switch__off, switch__on } switch_e;

void producer(void *p);
void consumer(void *p);
bool get_switch_input_from_switch0();
static QueueHandle_t switch_queue;
void main(void) {
  gpio0__set_as_input(30);
  // TODO: Create your tasks
  xTaskCreate(producer, "producer", 4096 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(consumer, "consumer", 4096 / sizeof(void *), NULL, PRIORITY_HIGH, NULL);

  // TODO Queue handle is not valid until you create it
  switch_queue = xQueueCreate(1, sizeof(switch_e)); // Choose depth of item being our enum (1 should be okay
  // for this example)

  vTaskStartScheduler();
}
// TODO : Create this task at PRIORITY_LOW
void producer(void *p) {
  while (1) {
    // This xQueueSend() will internally switch context to "consumer" task because it is higher priority than this
    // "producer" task Then, when the consumer task sleeps, we will resume out of xQueueSend()and go over to the next
    // line

    // TODO: Get some input value from your board
    const switch_e switch_value = get_switch_input_from_switch0();

    // TODO: Print a message before xQueueSend()
    // Note: Use printf() and not fprintf(stderr, ...) because stderr is a polling printf
    printf("Producer Task: Before Queue Send\n");
    xQueueSend(switch_queue, &switch_value, 0);
    // TODO: Print a message after xQueueSend()
    printf("Producer Task: After Queue Send\n");

    vTaskDelay(1000);
  }
}

// TODO: Create this task at PRIORITY_HIGH
void consumer(void *p) {
  switch_e switch_value;
  while (1) {
    // TODO: Print a message before xQueueReceive()
    printf("Consumer Task: Before Queue Receive\n");
    xQueueReceive(switch_queue, &switch_value, portMAX_DELAY);
    printf("Consumer Task: After Queue Receive\n");
    // TODO: Print a message after xQueueReceive()
  }
}

bool get_switch_input_from_switch0() { return gpio0__get_level(30); }
